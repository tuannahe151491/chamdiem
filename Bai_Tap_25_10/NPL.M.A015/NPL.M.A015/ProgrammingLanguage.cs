﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015
{
    internal class ProgrammingLanguage
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }

        public ProgrammingLanguage()
        {
        }

        public ProgrammingLanguage(int languageID, string languageName)
        {
            LanguageID = languageID;
            LanguageName = languageName;
        }
    }
}
