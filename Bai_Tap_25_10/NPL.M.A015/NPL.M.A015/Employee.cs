﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015
{
    internal class Employee
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public DateTime HiredDate { get; set; }
        public bool Status { get; set; }
        public int DepartmentID { get; set; }
        public Employee()
        {
        }
        public Employee(int employeeID, string employeeName, int age, string address, DateTime hiredDate, bool status, int departmentID)
        {
            EmployeeID = employeeID;
            EmployeeName = employeeName;
            Age = age;
            Address = address;
            HiredDate = hiredDate;
            Status = status;
            DepartmentID = departmentID;
        }
    }
}
