﻿// See https://aka.ms/new-console-template for more information

using Problem03;
using System.ComponentModel.DataAnnotations;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validate validate = new Validate();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("Chương trình quản lý sinh viên");
    Console.WriteLine("1.Thêm sinh viên");
    Console.WriteLine("2.Hiển thị Certificate");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 2);

    switch (luaChon)
    {
        case 1:
            service.AddStudent();
            break;
        case 2:
            service.ShowStudent();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 2);
