﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_3
{
    internal enum GraduateLevel
    {
        Excellent,
        VeryGood,
        Good,
        Average,
        Failed
    }
}
