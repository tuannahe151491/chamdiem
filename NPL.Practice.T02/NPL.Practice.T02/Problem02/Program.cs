﻿// See https://aka.ms/new-console-template for more information
static int[] sumOfArr(int[] arr)
{
    int[] sumArr = new int[arr.Length];
    sumArr[0] = arr[0];
    for (int i = 1; i < arr.Length; ++i)
    {
        sumArr[i] = sumArr[i - 1] + arr[i];
    }

    return sumArr;
}

static int FinaMaxSubArray(int[] array, int length)
{
    int maxValue = Int32.MinValue;
    int[] sumArr = sumOfArr(array);
    for (int i = length - 1; i < array.Length; ++i)
    {
        int sum;
        if (i - length >= 0)
        {
            sum = sumArr[i] - sumArr[i - length];
        }
        else sum = sumArr[i];
        if (sum > maxValue) maxValue = sum;
    }
    return maxValue;
}

Console.WriteLine("Input length of array:");
int n = int.Parse(Console.ReadLine());
Console.WriteLine("input arr");
int[] arr = new int[n];
for (int i = 0; i < n; ++i)
{
    arr[i] = int.Parse(Console.ReadLine());
}
Console.WriteLine("Input subLength");
int subLength = int.Parse(Console.ReadLine());
Console.WriteLine(FinaMaxSubArray(arr, subLength));

