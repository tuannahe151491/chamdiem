﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace De4
{
    internal class Validation
    {
        public string InputString(string message, string pattern)
        {
            string input = string.Empty;
            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Dữ liệu không hợp lệ");
                    continue;
                }
                else
                {
                    break;
                }
            }

            return input;
        }

        public DateTime InputDate(String message)
        {
            string input = string.Empty;
            DateTime date;

            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                try
                {
                    date = DateTime.ParseExact(input, "dd/MM/yyyy", null);
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Định dạng ngày không hợp lệ!");
                    continue;
                }
            }
            return date;
        }

        public int InputIntegerInRange(string message, int min, int max)
        {
            string input;
            int result;
            while (true)
            {
                try
                {
                    Console.Write(message);
                    input = Console.ReadLine();
                    result = Convert.ToInt32(input);
                    if (result < min || result > max)
                    {
                        Console.WriteLine("Dữ liệu không hợp lệ!");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.Write("Dữ liệu không hợp lệ!");
                }
            }
            return result;
        }
        public int InputPostiveInteger(string message)
        {
            string input;
            int result;
            while (true)
            {
                try
                {
                    Console.Write(message);
                    input = Console.ReadLine();
                    result = Convert.ToInt32(input);
                    if (result <= 0)
                    {
                        Console.WriteLine("Nhập số lớn hơn 0!");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.Write("Nhập số lớn hơn 0!");
                }
            }
            return result;
        }

        public double InputDoubleInRange(string message, double min, double max)
        {
            double result;
            string input;
            Console.Write(message);
            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                    result = Convert.ToDouble(input);
                    if (result < min || result > max)
                    {
                        Console.WriteLine("Vui lòng nhập trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Dữ liệu không hợp lệ");
                }

            }
            return result;
        }
        public double InputPositiveDouble(string message)
        {
            double result;
            string input;
            Console.Write(message);
            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                    result = Convert.ToDouble(input);
                    if (result <= 0)
                    {
                        Console.WriteLine("Nhập số lớn hơn 0!");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Dữ liệu không hợp lệ");
                }

            }
            return result;
        }

        public double InputNoNegativeDouble(string message)
        {
            double result;
            string input;
            Console.Write(message);
            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                    result = Convert.ToDouble(input);
                    if (result < 0)
                    {
                        Console.WriteLine("Nhập số không âm!");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Nhập số không âm!");
                }

            }
            return result;
        }

        public bool InputYesNo(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                if (input.ToLower().Equals("y"))
                {
                    return true;
                }
                else if (input.ToLower().Equals("n"))
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Nhập Y/y hoặc N/n!");
                }
            }
        }
    }
}
