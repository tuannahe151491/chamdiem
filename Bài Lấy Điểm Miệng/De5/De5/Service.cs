﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De5
{
    internal class Service
    {
        public List<Covid> listCovids= new List<Covid>();

        Validate validate = new Validate();
        public void AddCovid()
        {
            while (true)
            {
                string maCovid= validate.InputString("Mã Covid: ", "[A-Za-z0-9]+"); ;
                string name = validate.InputString("Tên: ", "[A-Za-z\\s]+");
                int namPhatHien = validate.InputInt("Năm phát hiện", 1000, 3000);
                listCovids.Add(new Covid(maCovid, name, namPhatHien));
                Console.WriteLine("Bạn có muốn nhập tiếp không: ");
                if (!validate.CheckInputYN())
                {
                    return;
                }
            }
        }

        public void ShowCovid()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã", "Tên", "Năm phát hiện"));
            foreach (Covid covid in listCovids)
            {
                covid.InThongTin();
            }
        }

        public void ShowCovidStartCO()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã", "Tên", "Năm phát hiện"));
            foreach (Covid covid in listCovids)
            {
                if (covid.Ten.StartsWith("CO"))
                {
                    covid.InThongTin();
                }
            }
        }

        public void SortByYear()
        {
            for(int i= 0; i < listCovids.Count; i++)
            {
                for(int j= i; j< listCovids.Count; j++)
                {
                    if (listCovids[i].NamPhatHien> listCovids[j].NamPhatHien)
                    {
                        (listCovids[j], listCovids[i]) = (listCovids[i], listCovids[j]);
                    }
                }
            }
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã", "Tên", "Năm phát hiện"));
            foreach (Covid covid in listCovids)
            {
                    covid.InThongTin();
            }
        }

        public void Init()
        {
            listCovids.Add(new Covid("123", "COvid1", 2023));
            listCovids.Add(new Covid("523", "COvid2", 2021));
            listCovids.Add(new Covid("173", "CHvid3", 2023));
            listCovids.Add(new Covid("163", "Covid4", 2022));

        }



    }
}
