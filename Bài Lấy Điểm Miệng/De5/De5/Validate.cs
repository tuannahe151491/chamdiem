﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace De5
{
    internal class Validate
    {
        public string InputString(string message, string pattern)
        {
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Dữ liệu không hợp lệ");
                    continue;
                }
                return input;
            }

        }

        public DateTime InputDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                try
                {
                    DateTime date = DateTime.ParseExact(input, "dd/MM/yyyy", null);
                    return date;
                }
                catch
                {
                    Console.WriteLine("Định dạng ngày không hợp lệ");
                }
            }

        }

        public int InputInt(string mess, int min, int max)
        {
            Console.WriteLine(mess);
            while (true)
            {
                try
                {
                    int number = int.Parse(Console.ReadLine());
                    // Kiểm tra khoảng giá trị của số
                    if (number < min || number > max)
                    {
                        Console.WriteLine("Vui lòng nhập số nằm trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    return number;
                }
                catch
                {
                    Console.WriteLine("Vui lòng nhập số nguyên: ");
                }
            }
        }

        public double InputDouble(string mess, double min, double max)
        {
            Console.WriteLine(mess);
            while (true)
            {
                try
                {
                    double number = double.Parse(Console.ReadLine());
                    if (number < min || number > max)
                    {
                        Console.WriteLine("Vui lòng nhập số nằm trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    return number;
                }
                catch
                {
                    Console.WriteLine("Vui lòng nhập số thập phân: ");
                }
            }
        }

        public float InputFloat(string mess, float min, float max)
        {
            Console.WriteLine(mess);
            while (true)
            {
                try
                {
                    float number = float.Parse(Console.ReadLine());
                    if (number < min || number > max)
                    {
                        Console.WriteLine("Vui lòng nhập số nằm trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    return number;
                }
                catch
                {
                    Console.WriteLine("Vui lòng nhập số thập phân: ");
                }
            }
        }

        public bool CheckInputYN()
        {
            while (true)
            {
                string result = Console.ReadLine();
                if (result.Equals("Y", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                else if (result.Equals("N", StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
                Console.WriteLine("Vui lòng nhập y/Y hoặc n/N.");
                Console.WriteLine("Nhập lại: ");
            }
        }

        public bool CheckNameExist(List<Covid> covids, string name)
        {
            foreach (Covid covid in covids)
            {
                if (covid.Ten.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Tên đã tồn tại");
                    return false;
                }
            }
            return true;
        }
    }

}
