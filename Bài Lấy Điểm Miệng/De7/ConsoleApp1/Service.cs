﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Service
    {
        Validate validate= new Validate();
        List<Student> listStudents= new List<Student>();

        public void AddStudent()
        {
            while (true)
            {
                int maSV;
                while (true)
                {
                    maSV = validate.InputInt("Mã Sinh Viên: ",0, 10000);
                    if (validate.CheckIdExist(listStudents, maSV))
                    {
                        break;
                    }
                }
                Console.WriteLine("Name:");
                string name = validate.ValidateName();
                int age = validate.InputInt("Tuổi", 0, 200);
                string nganhHoc= validate.InputString("Ngành học viên: ", "[A-Za-z\\s]+");
                listStudents.Add(new Student(maSV, name, age, nganhHoc));

                Console.WriteLine("Bạn có muốn nhập tiếp không: ");
                if (!validate.CheckInputYN())
                {
                    return;
                }
            }
        }

        public void ShowStudent()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -10} {3, -10}", "Id", "Tên", "Tuổi", "Ngành"));
            foreach (Student student in listStudents)
            {
                student.InThongTin();
            }
        }

        public void ShowStudentWithYear()
        {
            List<StudentDetail> studentDetails = new List<StudentDetail>();

            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -10} {3, -10} {4, -10}", "Id", "Tên", "Tuổi", "Ngành", "Năm Sinh"));
            foreach (Student student in listStudents)
            {
                studentDetails.Add(new StudentDetail(student.Id, student.Ten, student.Tuoi, student.Nganh));
            }
            foreach (StudentDetail student in studentDetails)
            {
                student.InThongTin();
            }
        }

        public void DeleteById()
        {
            int id= validate.InputInt("Mã Sinh Viên: ", 0, 10000);
            int flag = 0;
            for(int i= 0; i< listStudents.Count; i++)
            {
                if (listStudents[i].Id == id)
                {
                    flag = 1;
                    listStudents.Remove(listStudents[i]);
                    break;
                }
            }
            if(flag == 1)
            {
                Console.WriteLine("Xóa thành công");
            }
            else
            {
                Console.WriteLine("Không tìm thấy sinh viên");
            }
        }

        public void Init()
        {
            listStudents.Add(new Student(1, "ninh", 20, "CNTT"));
            listStudents.Add(new Student(2, "ninh", 22, "CNTT"));
            listStudents.Add(new Student(3, "ninh", 17, "CNTT"));

        }

    }
}
