﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace De3
{
    internal class Validate
    {
        public string InputString(string message, string pattern)
        {
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Dữ liệu không hợp lệ");
                    continue;
                }
                return input;
            }
        }

        public DateTime InputDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                try
                {
                    DateTime date = DateTime.ParseExact(input, "dd/MM/yyyy", null);
                    return date;
                }
                catch
                {
                    Console.WriteLine("Định dạng ngày không hợp lệ.");
                }
            }
        }

        public int InputInt(string message, int min, int max)
        {
            Console.WriteLine(message);
            while (true)
            {
                try
                {
                    int number = int.Parse(Console.ReadLine());
                    // Kiểm tra khoảng giá trị của số
                    if (number < min || number > max)
                    {
                        Console.WriteLine("Vui lòng nhập trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    return number;
                }
                catch
                {
                    Console.WriteLine("Vui lòng nhập số nguyên: ");
                }
            }
        }

        public double InputDouble(string message, double min, double max)
        {
            Console.WriteLine(message);
            while (true)
            {
                try
                {
                    double number = double.Parse(Console.ReadLine());
                    if (number < min || number > max)
                    {
                        Console.WriteLine("Vui lòng nhập trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    return number;
                }
                catch
                {
                    Console.WriteLine("Vui lòng nhập số thập phân: ");
                }
            }
        }

        public float InputFloat(string message, float min, float max)
        {
            Console.WriteLine(message);
            while (true)
            {
                try
                {
                    float number = float.Parse(Console.ReadLine());
                    if (number < min || number > max)
                    {
                        Console.WriteLine("Vui lòng nhập trong khoảng từ " + min + " đến " + max + ": ");
                        continue;
                    }
                    return number;
                }
                catch
                {
                    Console.WriteLine("Vui lòng nhập số thập phân: ");
                }
            }
        }

        public bool CheckInputYN()
        {
            while (true)
            {
                string result = Console.ReadLine();
                if (result.Equals("Y") || result.Equals("y"))
                {
                    return true;
                }
                else if (result.Equals("N") || result.Equals("n"))
                {
                    return false;
                }
                Console.WriteLine("Vui lòng nhập y/Y hoặc n/N.");
                Console.WriteLine("Nhập lại: ");
            }
        }

        public bool CheckNameExist(List<MayTinh> mayTinhs, string name)
        {
            foreach (MayTinh maytinh in mayTinhs)
            {
                if (maytinh.Ten.Equals(name))
                {
                    Console.WriteLine("Tên máy tính đã tồn tại");
                    return false;
                }
            }
            return true;
        }
    }
}
