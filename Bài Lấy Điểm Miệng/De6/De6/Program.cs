﻿using De6;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validate validate = new Validate();
int choice;

Service service = new Service();
service.Init();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất danh sách thêm năm sinh");
    Console.WriteLine("4. Xoá đối tượng theo mã HS");
    Console.WriteLine("0. Thoát");
    choice = validate.InputIntegerInRange("Chọn chức năng: ", 0, 5);
    switch (choice)
    {
        case 1:
            service.AddStudent();
            break;
        case 2:
            service.ShowStudent();
            break;
        case 3:
            service.ShowStudentNamSinh();
            break;
        case 4:
            service.DeleteStudentById();
            break;
        case 5:
            break;
        default:
            break;
    }
} while (choice > 0 && choice <= 4);
