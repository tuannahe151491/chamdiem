﻿using De2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De2
{
    internal class Service
    {
        Validation validation = new Validation();
        List<GiaoVien> GiaoVienList = new List<GiaoVien>();
        public int InputId()
        {
            int id ;
            bool check = true;
            while (true)
            {
                id = validation.InputPostiveInteger("Nhap ID: ");
                foreach (GiaoVien giaovien in GiaoVienList)
                {
                    check = true;
                    if (id == giaovien.Id)
                    {
                        check = false;
                        Console.WriteLine("ID đã tồn tại");
                        break;
                    }
                }
                if(check)
                {
                    return id;
                }
            }
        }

        public void AddTeacher()
        {
            do
            {
                int id = InputId();
                string ten = validation.InputString("Nhập tên: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                double soGioDay = validation.InputNoNegativeDouble("Nhập số giờ dạy: ");
                GiaoVienList.Add(new GiaoVien(id, ten, soGioDay));
            }
            while (validation.InputYesNo("Có muốn nhập tiếp không? (Y/y N/n): "));
        }

        public void ShowTeacher()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -30}{2, -20}", "ID", "Họ và tên", "Số giờ dạy"));
            foreach(GiaoVien giaoVien in GiaoVienList)
            {
                giaoVien.InThongTin();
            }
        }

        public void XuatGiaoVienCoGioDayTheoKhoang()
        {
            double min;
            double max;
            while (true)
            {
                min = validation.InputNoNegativeDouble("Nhập số giờ dạy min: ");
                max = validation.InputNoNegativeDouble("Nhập số giờ dạy max: ");
                if(min > max)
                {
                    Console.WriteLine("Nhập min <= max!");
                }
                else
                {
                    break;
                }
            }
            bool check = false;
            Console.WriteLine(string.Format("{0, -5}{1, -30}{2, -20}", "ID", "Họ và tên", "Số giờ dạy"));
            foreach (GiaoVien giaoVien in GiaoVienList)
            {
                if(giaoVien.SoGioDay >= min && giaoVien.SoGioDay <= max)
                {
                    giaoVien.InThongTin();
                    check = true;
                }
            }
            if(check == false)
            {
                Console.WriteLine("Không có giáo viên nào !");
            }
        }


        public void DeleteById()
        {
            int id;
            id = validation.InputPostiveInteger("Nhập ID cần xoá: ");
            bool check = false;
            foreach (GiaoVien giaoVien in GiaoVienList)
            {
                if (giaoVien.Id == id)
                {
                    GiaoVienList.Remove(giaoVien);
                    Console.WriteLine("Đã xoá giáo viên với ID = " + id);
                    check = true;
                    break;
                }
            }
            if (check == false)
            {
                Console.WriteLine("Không tìm thấy giáo viên nào!");
            }

        }

        public void KeThua()
        {
            GiaoVienPoly giaoVienPoly = new GiaoVienPoly();
            Console.WriteLine(string.Format("{0, -5}{1, -30}{2, -20}{3, -15}", "ID", "Họ và tên", "Số giờ dạy", "Ngành Dạy"));
            giaoVienPoly.InThongTin();
        }

        public void Init()
        {
            GiaoVienList.Add(new GiaoVien(1, "Nguyễn Anh Tuấn", 22));
            GiaoVienList.Add(new GiaoVien(2, "Nguyễn Anh Tuấn", 23));
            GiaoVienList.Add(new GiaoVien(3, "Anh Tuấn", 24));
        }
    }
}
