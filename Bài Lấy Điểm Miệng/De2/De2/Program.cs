using De2;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validation validation = new Validation();
int choice;

Service service = new Service();
service.Init();
do
{
    Console.WriteLine("-----------------------------------------");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất danh sách GV có giờ dạy theo khoảng");
    Console.WriteLine("4. Xoá đối tượng theo ID");
    Console.WriteLine("5. Kế thừa");
    Console.WriteLine("0. Thoát");
    choice = validation.InputIntegerInRange("Chọn chức năng: ", 0, 5);
    switch (choice)
    {
        case 1:
            service.AddTeacher();
            break;
        case 2:
            service.ShowTeacher();
            break;
        case 3:
            service.XuatGiaoVienCoGioDayTheoKhoang();
            break;
        case 4:
            service.DeleteById();
            break;
        case 5:
            service.KeThua();
            break;
        default:
            break;
    }
} while (choice >0 && choice <= 5);
Console.ReadKey();

