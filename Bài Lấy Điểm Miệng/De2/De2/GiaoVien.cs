﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De2
{
    internal class GiaoVien
    {
        public int Id {  get; set; }
        public string Ten {  get; set; }
        public double SoGioDay {  get; set; }

        public GiaoVien()
        {
        }

        public GiaoVien(int id, string ten, double soGioDay)
        {
            this.Id = id;
            this.Ten = ten;
            this.SoGioDay = soGioDay;
        }

        public virtual void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -30}{2, -5}", this.Id, this.Ten, this.SoGioDay));        
        }
    }
}
