﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace De1
{
    internal class Service
    {
        List<SinhVien> listSinhViens = new List<SinhVien>() { };
        Validate validate = new Validate();
        public void AddSinhVien()
        {
            while (true)
            {
                string maSV;
                while (true)
                {
                    maSV = validate.InputString("Mã Sinh Viên: ", "^[A-Za-z0-9]+$");
                    if (validate.CheckIdExist(listSinhViens, maSV))
                    {
                        break;
                    }
                }
                string name = validate.InputString("Tên sinh viên: ", "[A-Za-z\\s]+");
                int birthDate = validate.InputInt("Năm sinh", 1000, 3000);
                listSinhViens.Add(new SinhVien(maSV, name, birthDate));

                Console.WriteLine("Bạn có muốn nhập tiếp không: ");
                if (!validate.CheckInputYN())
                {
                    return;
                }
            }
        }

        public void ShowSinhVien()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh"));
            foreach (SinhVien sinhvien in listSinhViens)
            {
                if (!(sinhvien is SinhVienUDPM))
                {
                    Console.WriteLine(sinhvien.InThongTin());
                }

            }

            int count = 0;
            foreach (SinhVien sinhVien in listSinhViens)
            {
                if (sinhVien is SinhVienUDPM)
                {
                    count++;
                    if (count == 1)
                    {
                        Console.WriteLine("Sinh Viên UDPM");
                        Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15} {3, -10}{4, -5}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh", "Điển Java", "Điểm C#"));
                    }
                    SinhVienUDPM sinhVienUDPM = (SinhVienUDPM)(sinhVien);
                    Console.WriteLine(sinhVienUDPM.InThongTin());
                }
            }
        }

        public void ListSinhVienHigher50Age()
        {
            int count1 = 0;
            DateTime date = DateTime.Now;
            foreach (SinhVien sinhvien in listSinhViens)
            {
                if (!(sinhvien is SinhVienUDPM) && (date.Year - sinhvien.NamSinh) >= 50)
                {
                    count1++;
                    if (count1 == 1)
                    {
                        Console.WriteLine("Danh Sách Sinh Viên");
                        Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh"));
                    }
                    Console.WriteLine(sinhvien.InThongTin());
                }

            }

            int count = 0;
            foreach (SinhVien sinhVien in listSinhViens)
            {
                if (sinhVien is SinhVienUDPM && (date.Year - sinhVien.NamSinh) >= 50)
                {
                    count++;
                    if (count == 1)
                    {
                        Console.WriteLine("Sinh Viên UDPM");
                        Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15} {3, -10}{4, -5}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh", "Điển Java", "Điểm C#"));
                    }
                    SinhVienUDPM sinhVienUDPM = (SinhVienUDPM)(sinhVien);
                    Console.WriteLine(sinhVienUDPM.InThongTin());
                }
            }

            if (count1 == 0 && count== 0)
            {
                Console.WriteLine("Không có sinh viên nào");
            }
        }

        public void SearchSinhVien()
        {
            int count1 = 0;
            string maSV = validate.InputString("Nhập mã Sinh Viên: ", "^[A-Za-z0-9]+$");
            foreach (SinhVien sinhvien in listSinhViens)
            {
                if (!(sinhvien is SinhVienUDPM) && sinhvien.MaSV.Equals(maSV))
                {
                    count1++;
                    if (count1 == 1)
                    {
                        Console.WriteLine("Sinh Viên UDPM");
                        Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15} {3, -10}{4, -5}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh", "Điển Java", "Điểm C#"));
                    }
                    Console.WriteLine(sinhvien.InThongTin());
                }

            }

            int count = 0;
            foreach (SinhVien sinhVien in listSinhViens)
            {
                if (sinhVien is SinhVienUDPM && sinhVien.MaSV.Equals(maSV))
                {
                    count++;
                    if (count == 1)
                    {
                        Console.WriteLine("Sinh Viên UDPM");
                        Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15} {3, -10}{4, -5}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh", "Điển Java", "Điểm C#"));
                    }
                    SinhVienUDPM sinhVienUDPM = (SinhVienUDPM)(sinhVien);
                    Console.WriteLine(sinhVienUDPM.InThongTin());
                }
            }

            if (count1 == 0 && count == 0)
            {
                Console.WriteLine("Không có sinh viên nào");
            }
        }

        public void InheritSinhVien()
        {

            string maSV;
            while (true)
            {
                maSV = validate.InputString("Mã Sinh Viên: ", "^[A-Za-z0-9]+$");
                if (validate.CheckIdExist(listSinhViens, maSV))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Mã sinh viên đã tồn tại");
                }
            }
            string name = validate.InputString("Tên sinh viên: ", "[A-Za-z\\s]+");
            int birthDate = validate.InputInt("Năm sinh", 1000, 3000);
            double diemJava = validate.InputDouble("Điểm Java", 0, 10);
            double diemCsharp = validate.InputDouble("Điểm C#", 0, 10);
            SinhVienUDPM sinhVien = new SinhVienUDPM(maSV, name, birthDate, diemJava, diemCsharp);
            listSinhViens.Add(sinhVien);
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -15} {3, -10}{4, -5}", "Mã sinh viên", "Tên sinh viên", "Năm Sinh", "Điển Java", "Điểm C#"));
            Console.WriteLine(sinhVien.InThongTin()); 
        }

        public void Init()
        {
            listSinhViens.Add(new SinhVien("123", "Tuan", 2002));
            listSinhViens.Add(new SinhVien("122", "Duy", 2002));
            listSinhViens.Add(new SinhVien("444", "Thanh", 2002));
            listSinhViens.Add(new SinhVien("445", "Phuc", 2002));

        }
    }
}
