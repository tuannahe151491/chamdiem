﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2
{
    internal static class ArrayExtension
    {

        public static void RemoveDuplicate(this int[] array)
        {
            List<int>  intList = new List<int>();


            foreach (int i in array)
            {
                if(intList.Contains(i) == false)
                {
                    intList.Add(i);
                }
            }

            Console.WriteLine("Mảng sau khi lọc trùng: ");
            Console.WriteLine(string.Join(" ", intList));
        }

        public static void RemoveDuplicate<T>(this T[] array)
        {
            List<T> intList = new List<T>();


            foreach (T i in array)
            {
                if (intList.Contains(i) == false)
                {
                    intList.Add(i);
                }
            }

            Console.WriteLine("Mảng sau khi lọc trùng: ");
            Console.WriteLine(string.Join(" ", intList));
        }
    }
}
