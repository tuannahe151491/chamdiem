﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    internal static class ArrayListExtensionStatic
    {
        // Phương thức mở rộng này đếm số lượng phần tử có kiểu dữ liệu int trong ArrayList.
        public static int CountInt(this ArrayList array)
        {
            return array.OfType<int>().Count();
        }

        // Phương thức mở rộng này đếm số lượng phần tử có kiểu dữ liệu là dataType trong ArrayList.
        public static int CountOf(this ArrayList array, Type dataType)
        {
            return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }

        // Phương thức mở rộng này đếm số lượng phần tử có kiểu dữ liệu là T trong ArrayList.
        public static int CountOf<T>(this ArrayList array)
        {
            return array.OfType<T>().Count();
        }

        // Phương thức mở rộng này trả về giá trị lớn nhất của kiểu T nếu T là kiểu số, ngược lại ném một ngoại lệ.
        public static T MaxOf<T>(this ArrayList array) where T : IComparable
        {
            var numericItems = array.OfType<T>();
            if (numericItems.Any())
            {
                return numericItems.Max();
            }
            throw new InvalidOperationException("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");
        }
    }
}
